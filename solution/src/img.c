#include "img.h"

struct image img_create_empty_one() {
	return (struct image) {0};
}

struct image img_create(uint64_t width, uint64_t height, struct pixel* data) {
	return (struct image) {.width = width, .height = height, .data = data};
}
