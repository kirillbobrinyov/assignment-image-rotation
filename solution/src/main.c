#include "bmp.h"
#include "img.h"
#include "rotation.h"
#include <stdbool.h>

int main( int argc, char** argv ) {

    if (argc == 3) {
		FILE* in = fopen(argv[1], "rb");
		FILE* out = fopen(argv[2], "ab");
		
		if (in == NULL || out == NULL) {
			if (in == NULL) fprintf(stderr, "Unable to open file %s \n", argv[1]);
			if (out == NULL) fprintf(stderr, "Unable to open file %s \n", argv[2]);
		} else {
			struct image img = img_create_empty_one();
			enum bmp_file_read_status bmp_reading_status = from_bmp(in, &img);
			
			if (bmp_reading_status != READ_SUCCESS) fprintf(stderr, "%s", bmp_read_messages[bmp_reading_status]);
			else {
				printf("%s", bmp_read_messages[bmp_reading_status]);
				struct image new_img = rotate(img);
				enum bmp_file_write_status ws = to_bmp( out, &new_img);
				
				if (ws != WRITE_SUCCESS) fprintf(stderr, "%s", bmp_write_messages[ws]);
				else printf("%s", bmp_write_messages[ws]);
				free(img.data);
				free(new_img.data);
			}
		}
		
		if (in != NULL) fclose(in);
		if (out != NULL) fclose(out);
	} else {
		printf("Usage example: %s <input_file>.bmp <output_file>.bmp\n", argv[0]);
	}
    
    return 0;
}


