#include "bmp.h"

struct __attribute__((packed)) bmp_header
{
	uint16_t bfType;
	uint32_t  bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t  biClrImportant;
};

static const struct bmp_header BMP_HEADER_INVALID = {0};
static const uint16_t BMP_SIGNATURE = 19778;
static const uint16_t BMP_PIXEL_SIZE_IN_BITS = 24; 
static const uint32_t BMP_DIB_HEADER_SIZE = 40;
static const uint32_t BMP_HEADER_SIZE = 54;
static const uint16_t BMP_PLANES_NUM = 1;

static uint32_t calc_padding(uint32_t width_in_pixels) {
	
	if (width_in_pixels % 4 != 0) return ((width_in_pixels * 3 / 4) + 1) * 4 - width_in_pixels * 3;
	return 0;
	
}

static uint32_t calc_img_size(const uint32_t width, const uint32_t height, const uint32_t padding) {
	
	return 3 * width * height + padding * height;
	
}

static struct bmp_header bmp_header_create(uint32_t width, uint32_t height, uint32_t padding) {
	
	const uint32_t img_size = calc_img_size(width, height, padding);
	return (struct bmp_header) {
		.bfType = BMP_SIGNATURE,
		.bfileSize = BMP_HEADER_SIZE + img_size,
		.bfReserved = 0,
		.bOffBits = BMP_HEADER_SIZE,
		.biSize = BMP_DIB_HEADER_SIZE,
		.biWidth = width,
		.biHeight = height,
		.biPlanes = BMP_PLANES_NUM,
		.biBitCount = BMP_PIXEL_SIZE_IN_BITS,
		.biCompression = 0,
		.biSizeImage = img_size,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0
	};
}

static struct bmp_header bmp_header_read(FILE* in) {
	
	struct bmp_header header = {0};
	if (fread(&header, sizeof(struct bmp_header), 1, in) == 1) return header;
	return BMP_HEADER_INVALID;
	
}

static enum bmp_file_write_status bmp_header_write(FILE* out, struct bmp_header header) {
	
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
	return WRITE_SUCCESS;
	
}

static enum bmp_file_read_status bmp_header_validate(const struct bmp_header* header) {
	
	if (header->bfType != BMP_SIGNATURE) return READ_INVALID_SIGNATURE;
	if (header->biBitCount != BMP_PIXEL_SIZE_IN_BITS) return READ_INVALID_PIXEL_SIZE;
	return READ_SUCCESS;
	
}


static struct pixel* bmp_pixels_read(FILE* file, const uint32_t width_in_pixels, const uint32_t height_in_pixels, const uint32_t padding) {
	
	struct pixel* array = malloc(sizeof(struct pixel) * width_in_pixels * height_in_pixels);
	uint32_t current_row = 0;
	while (current_row < height_in_pixels) {
		struct pixel* position = array + current_row * width_in_pixels;
		const size_t items_read = fread(position, sizeof(struct pixel), width_in_pixels, file);	
		if (items_read != width_in_pixels) {
			free(array);
			return NULL;
		}
		int fseek_success = fseek(file, padding, SEEK_CUR);
		if (!(fseek_success == 0) ) {
			free(array);
			return NULL;
		}
		current_row++;
	}
	return array;
	
}


static enum bmp_file_write_status bmp_pixels_write(FILE* file, struct pixel* array, const uint32_t width_in_pixels, const uint32_t height_in_pixels, const uint32_t padding) {
	
	uint32_t current_row = 0;
	while (current_row < height_in_pixels) {
		struct pixel* position = array + current_row * width_in_pixels;
		const size_t items_written = fwrite(position, sizeof(struct pixel), width_in_pixels, file);
		if (items_written != width_in_pixels) return WRITE_ERROR;
		size_t padding_written = fwrite(position, 1, padding, file);
		if (padding_written != padding) return WRITE_ERROR;
		current_row++;
	}
	return WRITE_SUCCESS;
	
}

enum bmp_file_read_status from_bmp( FILE* file, struct image* img) {
	
	const struct bmp_header header = bmp_header_read(file);
	enum bmp_file_read_status header_validation = bmp_header_validate(&header);
	if (header_validation != READ_SUCCESS) return header_validation;
	struct pixel* array = bmp_pixels_read(file, header.biWidth, header.biHeight, calc_padding(header.biWidth));
	if (array == NULL) return READ_INVALID_PIXEL_ARRAY;
	img->height = (uint64_t)  header.biHeight;
	img->width = (uint64_t) header.biWidth;
	img->data = array;
	return READ_SUCCESS;
}

enum bmp_file_write_status to_bmp( FILE* file, struct image const* img) {
	
	uint32_t padding = calc_padding((uint32_t) img->width);
	struct pixel* array = img->data;
	struct bmp_header header = bmp_header_create(img->width, img->height, padding);
	enum bmp_file_write_status status = bmp_header_write(file, header);
	if (status != WRITE_SUCCESS) return WRITE_ERROR;
	status = bmp_pixels_write(file, array, img->width, img->height, padding);
	return status;
	
}



