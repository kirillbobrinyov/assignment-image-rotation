#include "rotation.h"

struct image rotate( struct image const source ) {
	
	const uint64_t array_size =  source.height * source.width;
	struct pixel* new_data = malloc(sizeof(struct pixel) * array_size);
	for (uint64_t i = 0; i < array_size; i++) {
		const uint64_t new_x = source.height - i / source.width - 1;
		const uint64_t new_y = i % source.width;
		new_data[new_y * source.height + new_x] = source.data[i];
	}
	return img_create(source.height, source.width, new_data);
	
}

