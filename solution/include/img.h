#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef IMG_H
#define IMG_H

struct image {
	uint64_t width, height;
	struct pixel* data;
};

struct __attribute__((packed)) pixel {
	uint8_t b, g, r; 
};

struct image img_create_empty_one();

struct image img_create(uint64_t width, uint64_t height, struct pixel* data);

#endif
