#include "img.h"
#include <stdint.h>
#include <malloc.h>
#include <stdio.h>

#ifndef BMP_H
#define BMP_H

enum bmp_file_read_status {
 READ_SUCCESS = 0,
 READ_INVALID_SIGNATURE,
 READ_INVALID_PIXEL_ARRAY,
 READ_INVALID_PIXEL_SIZE
};

enum bmp_file_write_status {
 WRITE_SUCCESS = 0,
 WRITE_ERROR
};

static const char* bmp_read_messages[] = {
	[READ_SUCCESS] = "Successfully read\n",
	[READ_INVALID_SIGNATURE] = "Invalid signature\n",
	[READ_INVALID_PIXEL_ARRAY] = "Invalid pixel array\n",
	[READ_INVALID_PIXEL_SIZE] = "Invalid number of bytes for pixel\n"
};

static const char* bmp_write_messages[] = {
	[WRITE_SUCCESS] = "Successfully written\n",
	[WRITE_ERROR] = "Error while writing to bmp file\n"
};


enum bmp_file_read_status from_bmp( FILE* file, struct image* img);

enum bmp_file_write_status to_bmp( FILE* file, struct image const* img );

#endif
